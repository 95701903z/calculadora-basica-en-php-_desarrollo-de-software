# Calculadora Básica en PHP _Desarrollo de software<!DOCTYPE html>
<html>
<head>
	<title>Login</title>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<link rel="stylesheet" type="text/css" href="css/estilos.css">

</head>
<body>
<div class="panel panel-primary">
		<div class="panel-heading text-center">
			<h2>CALCULADORA</h2></div>
			<form action="#" method="POST" >	
		<div class="panel-body">		
		<p>Nro1: <input type="text" class="form-control" name="txtNro1"/></p>
        <p>Nro2: <input type="text" class="form-control" name="txtNro2"/></p>
		</div>					
		<div class="panel-footer">		
		<legend>Operadores</legend>
		<p><input type="submit" name="btnSumar" value="Sumar" button class="btn btn-primary"/> 
		<input type="submit" name="btnRestar" value="Restar" button class="btn btn-warning"/>
		<input type="submit" name="btnMultiplicar" value="Multiplicar" button class="btn btn-primary"/>
		<input type="submit" name="btnDividir" value="Divicion" button class="btn btn-warning"/>
		<input type="submit" name="btnFactorial" value="Factorial" button class="btn btn-primary"/>
		<input type="submit" name="btnPotencia" value="Potencia" button class="btn btn-warning"/>
		<input type="submit" name="btnSeno" value="Seno" button class="btn btn-primary"/>
		<input type="submit" name="btnTangente" value="Tangente" button class="btn btn-warning"/>
		<input type="submit" name="btnPorcentaje" value="Porcentaje" button class="btn btn-warning"/>
		<input type="submit" name="btnRaiz" value="Raiz" button class="btn btn-warning"/>
		</p>
		</div>
		</form>		  
		<?php 
    if(isset($_POST))
    {
        // Llamar a la clase Calculadora
        include("calculadora.php");        
        $nro1 = $_POST['txtNro1'];
        $nro2 = $_POST['txtNro2'];
        if(isset($_POST['btnSumar']))
        {
            // Instanciar un objeto a traves de la clase
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $suma = $calculo->Sumar();
            echo "La suma de los numeros es: " , $suma;
        }
        else  
		if(isset($_POST['btnRestar']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $resta = $calculo->Restar();
            echo "La resta de los numeros es: " , $resta;
        }  
		else  
		if(isset($_POST['btnMultiplicar']))
        {
			$calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $mltpc = $calculo->Multiplicar();
            echo "La Multiplicacion  de los numeros es: " , $mltpc;
		}  
		else  
		if(isset($_POST['btnDividir']))
        {
			$calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $div = $calculo->Dividir();
            echo "La Division  de los numeros es: ", $div;
		}  
		else  
		if(isset($_POST['btnFactorial']))
        {
			$calculo = new Calculadora;
            $calculo->nro1 = $nro1;            
            $fact = $calculo->Factorial();
            echo "La resta de los numeros es: " , $fact;
		}  
		else  
		if(isset($_POST['btnPotencia']))
        {
			$calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $pot = $calculo->Potencia();
            echo "La Potencia  de los numeros es: " , $pot;
		}  
		else  
		if(isset($_POST['btnSeno']))
        {
			$calculo = new Calculadora;
			$calculo->nro1 = $nro1;
      		$sen = $calculo->Seno();
	  echo "El Seno  de los numeros es: " , $sen;
		}  
		else  
		if(isset($_POST['btnTangente']))
        {
			$calculo = new Calculadora;
			$calculo->nro1 = $nro1;
      		$tan = $calculo->Tangente();
	  echo "La tangente de los numeros es: " , $tan;
		}  
		else  
		if(isset($_POST['btnPorcentaje']))
        {
			$calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $porc = $calculo->Porcentaje();
            echo "El porcentaje  de los numeros es: " , $porc;
		}
		else  
		if(isset($_POST['btnRaiz']))
        {
			$calculo = new Calculadora;
			$calculo->nro1 = $nro1;
      		$raiz = $calculo->Raiz();
	  echo "La Raiz es: " , $raiz;
		}    
    }
    ?>


    
       <?php
    class Calculadora{
        // atributos
        public $nro1;
        public $nro2;
        // Metodos u operaciones
        public function Sumar()
        {
            return $this->nro1 + $this->nro2;
        }

        public function Restar()
        {
            return $this->nro1 - $this->nro2;
        }
        public function Multiplicar()
        {
            return $this->nro1 * $this->nro2;
        }
        public function Dividir()
        {
            if ($this->nro2==0)
            
                return "No se puede dividr entre 0";           
            else            
                return $this->nro1/$this->nro2;
            
        }
        private function Fact($nro)
        {
            if($nro == 0)
                return 1;
            else
                return $nro * $this->Fact($nro - 1);
        }

        public function Factorial()
        {
            return $this->Fact($this->nro1);
        }
        //potencia, seno y tangente
        public function Potencia()
        {
            $pr = pow($nro1, $nro2);
            return $pr;
        }
        public function Seno()
        {
            $pr = sin(deg2rad($nro1));
            return $pr;
        }
        public function Tangente()
        {
            $pr = tan(deg2rad($nro1));
      return $pr;
        }
        
        public function Porcentaje()
        {
            return (round($nro1/$nro2*100));
        }
        public function Raiz()
        {
           return (sqrt($nro1));
        }

    }

</body>
</html>


   
